#include <malloc.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
struct job {
/* Campo de enlace para la lista enlazada. */
	struct job* next;
	
/* Otros campos que describen el trabajo a realizar ... */
	int X;
};
/*Una lista vinculada de trabajos pendientes. */
struct job* job_queue;

/* Un mutex que protege job_queue. */
pthread_mutex_t job_queue_mutex = PTHREAD_MUTEX_INITIALIZER;

/* Un semáforo que cuenta el número de trabajos en la cola. */
sem_t job_queue_count;
/* Ejecute la inicialización única de la cola de trabajos. */
void initialize_job_queue ()
{
/* La cola está inicialmente vacía. */
	printf("\n\x1B[33mInicializamos la job_queue");
	job_queue = NULL;
/* Inicialice el semáforo que cuenta los trabajos en la cola.
El valor inicial debe ser cero. */
	sem_init (&job_queue_count, 0, 0);
}

void process_job(struct job* trabajo)
{
	printf("\n\x1B[32m------->proceso %ld trabajando..\x1B[33m",(long)pthread_self());
	//printf("in job: %d ..\n", trabajo->X);
	system("sleep 0.05");
}

/* Procese los trabajos en cola hasta que la cola esté vacía. */
void* thread_function (void* arg)
{
	while (1) {
		struct job* next_job;

/* Espera en el semáforo de cola de trabajo. Si su valor es 
positivo, indicando que la cola no está vacía, disminuya el 
recuento por 1. Si la cola está vacía, bloquee hasta que se 
enhebre un nuevo trabajo. */
		printf("\n\x1B[31msem_wait() llamado en proceso en hilo %ld\x1B[33m",(long)pthread_self());
		sem_wait (&job_queue_count);
/* Bloquear el mutex en la cola de trabajos. */
		pthread_mutex_lock (&job_queue_mutex);

/*Debido al semáforo, sabemos que la cola no está vacía. 
Obtenga el siguiente trabajo disponible. */
		next_job = job_queue;

/* Elimine este trabajo de la lista. */
		job_queue = job_queue->next;

/* Desbloquear el mutex en la cola de trabajos porque hemos 
terminado con la cola por ahora. */
		pthread_mutex_unlock (&job_queue_mutex);
		
/* Llevar a cabo el trabajo. */
		process_job (next_job);

/* limpiar */

		free (next_job);
	}
	return NULL;
}

/* Agregue un trabajo nuevo al frente de la cola de trabajos. */
void enqueue_job (int X)
{
	struct job* new_job;

/* Asignar un nuevo objeto de trabajo. */
	new_job = (struct job*) malloc (sizeof (struct job));
	new_job->X = X;
/* Establecer los otros campos de la estructura de trabajo aquí ... */


/* Bloquee el mutex en la cola de trabajos antes de acceder a él. */
	pthread_mutex_lock (&job_queue_mutex);
/* Coloque el nuevo trabajo en la cabecera de la cola. */
	new_job->next = job_queue;
	job_queue = new_job;
/* Publicar en el semáforo para indicar que hay otro trabajo disponible. 
Si los hilos están bloqueados, esperando en el semáforo, uno se 
desbloqueará para poder procesar el trabajo. */
	sem_post (&job_queue_count);

/* Desbloquee el mutex de la cola de trabajos. */
	pthread_mutex_unlock (&job_queue_mutex);
}
void * thread_function_job(void * arg2)
{
	while(1)
	{
		system("sleep 3"); 
		printf("\n\n--->encolando job<---\n");
		enqueue_job(1);		
		
	}
}
int main()
{
	printf("\e[1mEn este programa utilizamos la cabecera semaphore.h \npara evitar que hilos terminen su ejecucion y en vez \nde eso esperen hasta encontrar nuevos trabajos en \nla cola de trabajos..\nPresione ctrl+Z para terminar\n");
	
	initialize_job_queue();
	int cantidad_de_hilos = 3;
	pthread_t threads[cantidad_de_hilos];
	struct job* cola;
	cola = (struct job*)malloc(sizeof(struct job*));
	cola->next = NULL;
	cola->X = 0;
	job_queue = cola;
	int cont;
	struct job* nuevo;
	printf("\nPrimero se encolan 5 jobs");
	printf("\nGeneramos 3 hilos que se encargaran de los jobs..");
	printf("\nGeneramos 1 hilo que se encargara de encolar mas jobs cada 3 segundos..");	
	for (cont = 1; cont < 5 ; cont++)
	{	
		enqueue_job(cont);
	}
	
	for (cont = 0; cont < cantidad_de_hilos; cont++)
	{
		pthread_create (&(threads[cont]), NULL, thread_function, NULL);
			
	}
	pthread_t threads_job;
	pthread_create (&threads_job, NULL, thread_function_job, NULL);
	for (cont = 0; cont < cantidad_de_hilos; cont++)
	{	
		pthread_join(threads[cont], NULL);
		cont++;
	}
	printf("programa finalizado correctamente\n");
	return 0;
}



